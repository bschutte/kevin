import React, { Component } from "react";
import Header from "../Components/Landingpage";
import Showcase from "../Components/Showcase";
import TabComponent from "../Components/tabs_nav/TabComponent";
import FeaturesTab from "../Components/FeaturesTab";
import Kurser from "../Components/GratisKurser";
import AllKurser from "../Components/AlleKurser";
import QA from "../Components/Q&A";
import Footer from "../Components/Footer";

class Main extends Component {
  render() {
    return (
      <div>
        <Header />
        <Showcase />
        <TabComponent />
        <FeaturesTab />
        <Kurser />
        <AllKurser />
        <QA />
        <Footer />
      </div>
    );
  }
}

export default Main;
