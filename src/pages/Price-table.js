import React, { Component } from "react";
import Pricing from "../Components/Price-table/Price-table";
import Navbar from "../Components/Navbar";

class PriceTable extends Component {
  render() {
    return (
      <div>
        <Navbar></Navbar>
        <Pricing></Pricing>
      </div>
    );
  }
}

export default PriceTable;
