import React, { Component } from "react";
import logo from "../svg/testlogo2.svg";
import { NavLink, Link } from "react-router-dom";
import { Button } from "./Buttons/Button";
import styled from "styled-components";
import { Icon } from "react-icons-kit";
import { ic_keyboard_arrow_right } from "react-icons-kit/md/ic_keyboard_arrow_right";
import Facebook from "./Facebook/Facebook";
// Media query
import { generateMedia } from "styled-media-query";
import Navbar from "../Components/Navbar";

class Header extends Component {
  render() {
    return (
      <HeaderComponent className="header-container">
        <Navbar></Navbar>
        {/* Header Content */}
        <div className="header-content">
          <Title>Enklere Matte</Title>
          <Subtitle>Når som helst, hvor som helst</Subtitle>
          <Button className="main-offer-btn">
            <Link className="LinkTxt" to="/priser">
              Prøv nå
              <Icon className="Icon" icon={ic_keyboard_arrow_right} size={37} />
            </Link>
          </Button>
        </div>
      </HeaderComponent>
    );
  }
}

export default Header;

const customMedia = generateMedia({
  largeDesktop: "1350px",
  medDesktop: "1150px",
  tablet: "960px",
  smallTablet: "740px",
  mobile: "46em"
});

// Header Container
const HeaderComponent = styled.div`
      padding-top: 5px;

      ${customMedia.greaterThan("largeDesktop")`
        height: 85vh;
      `}
      ${customMedia.between("medDesktop", "largeDesktop")`
        height: 90vh;
      `}
      ${customMedia.between("smallTablet", "medDesktop")`
        height: 100vh; 
      `}


    // Header Content 
    .header-content {
        width: 65%;
        position: relative;
        margin: 4.5rem auto 0;
        display: flex;
        justify-content: center;
        align-content: center;
        text-align: center;
        flex-direction: column;
        z-index: 1;
        ${customMedia.lessThan("smallTablet")`
        display: grid;
        grid-template-rows: repeat(3, 60px);
        margin-top: 8rem;
        `}
        ${customMedia.greaterThan("tablet")`
          margin: -220px auto 0;
          `}
        ${customMedia.greaterThan("largeDesktop")`
          padding-top: 100px;
        `}

    }


    .LinkTxt {
      color: #fff;
      text-decoration: none;
    }

    .Icon svg {
        color: #ff;

        vertical-align: bottom;
        margin-left: 1.5rem;
        ${customMedia.lessThan("smallTablet")`
        display:none !important;
        margin-left: 2px;
        ${customMedia.greaterThan("Tablet")`
        margin-right: 20px;`}
        `}
    }


    .Button {
      color: #ff;
    }

    .main-offer-btn {
        ${customMedia.lessThan("largeDesktop")`
        margin: 0 33%;
        font-size: 1.5rem;
        `}
        ${customMedia.lessThan("mediumDesktop")`
        margin: 0 25%;
        font-size: 1.5rem;
        `}
        ${customMedia.lessThan("tablet")`
        margin: 0 20%;
        font-size: 1.3rem;
        `}
        ${customMedia.greaterThan("smallTablet")`
        font-size: 23px;
        padding: 20px 20px 20px 65px;
        `}
        ${customMedia.greaterThan("largeDesktop")`
        margin: 0 33%;
        font-size: 26px;
        letter-spacing: 1.6px;        
        padding: 20px 35px 20px 65px;
        `}
    }


    .main-offer-btn:hover {
      transition: 0.5s;
      transform: scale(1.1);
    }
`;

// Main Title
const Title = styled.h1`
  margin: 10rem 0 1.2rem;
  color: #fff;
  font-size: 80px;
  font-weight: 700;
  line-height: 1.1em;
  ${customMedia.lessThan("tablet")`
    font-size: 2.6rem;
    margin: 0 0 1.2rem;
    `}
  ${customMedia.lessThan("medDesktop")`
    font-size: 40px;
    margin: 0 0 1rem;
  `}
  ${customMedia.lessThan("largeDesktop")`
    font-size: 40px;
    margin: 0 0 1rem;
  `}
  ${customMedia.greaterThan("tablet")`
    font-size: 65px;
  `}
  ${customMedia.greaterThan("largeDesktop")`
  margin: 10rem 0 1.2rem;
  font-size: 80px;
  font-weight: 700;
  line-height: 1.1em;`}
`;

// Subtitle
const Subtitle = styled.h2`
  font-weight: 400;
  font-size: 1.875rem;
  color: #fff;
  line-height: 1.25em;
  margin: 0 0 1.875rem;
  text-transform: default;
  ${customMedia.lessThan("smallTablet")`
    font-size: 1.4rem;
    padding-top: 2rem;
    `}
  ${customMedia.greaterThan("tablet")`
    font-size: 20px;
    `}
    ${customMedia.greaterThan("largeDesktop")`
    font-size: 26px;
    `}
`;
