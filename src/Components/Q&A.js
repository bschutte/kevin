import React, { Component } from "react";
import styled from "styled-components";
import { Icon } from "react-icons-kit";
import { chevronDown } from "react-icons-kit/fa/chevronDown";
import { chevronUp } from "react-icons-kit/fa/chevronUp";

class QA extends Component {
  render() {
    return (
      <Body>
        <section>
          <div className="qa-container">
            <span className="mainTitle">Spørsmål og svar</span>
            <div className="accordion">
              <div className="accordion-item" id="question1">
                <a className="accordion-link" href="#question1">
                  Er det gratis å prøve AMatte?
                  <Icon
                    className="div-icon-add"
                    icon={chevronDown}
                    size={15}
                  ></Icon>
                  <Icon
                    className="div-icon-remove"
                    icon={chevronUp}
                    size={15}
                    class="div-icon-remove"
                  ></Icon>
                </a>
                <div className="awnser">
                  <p>
                    Ja, du får tilgang til første del av de fleste kursene våre
                    hos EnkelMatte når du registrer deg helt gratis. På den
                    måten kan du prøve ut kursene før du betaler for å få
                    tilgang til hele kurset du ønsker.
                  </p>
                </div>
              </div>
              <div className="accordion-item" id="question2">
                <a className="accordion-link" href="#question2">
                  Hva lærer jeg meg med AMatte?
                  <Icon
                    className="div-icon-add"
                    icon={chevronDown}
                    size={15}
                  ></Icon>
                  <Icon
                    class="div-icon-remove"
                    icon={chevronUp}
                    size={15}
                  ></Icon>
                </a>
                <div className="awnser">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Integer aliquam sem vitae nibh hendrerit eleifend vitae
                    aliquet ligula. Mauris rutrum elit ac sapien mattis, at
                    imperdiet nunc commodo.
                  </p>
                </div>
              </div>
              <div className="accordion-item" id="question3">
                <a className="accordion-link" href="#question3">
                  Får jeg bedre karakter om jeg bruker AMatte?
                  <Icon
                    className="div-icon-add"
                    icon={chevronDown}
                    size={15}
                  ></Icon>
                  <Icon
                    class="div-icon-remove"
                    icon={chevronUp}
                    size={15}
                  ></Icon>
                </a>
                <div className="awnser">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Integer aliquam sem vitae nibh hendrerit eleifend vitae
                    aliquet ligula. Mauris rutrum elit ac sapien mattis, at
                    imperdiet nunc commodo.
                  </p>
                </div>
              </div>
              <div className="accordion-item" id="question4">
                <a className="accordion-link" href="#question4">
                  Hvem holder kursene hos AMatte? Kan jeg være sikker på at de
                  kan faget?
                  <Icon
                    className="div-icon-add"
                    icon={chevronDown}
                    size={15}
                  ></Icon>
                  <Icon
                    icon={chevronUp}
                    size={15}
                    class="div-icon-remove"
                  ></Icon>
                </a>
                <div className="awnser">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Integer aliquam sem vitae nibh hendrerit eleifend vitae
                    aliquet ligula. Mauris rutrum elit ac sapien mattis, at
                    imperdiet nunc commodo.
                  </p>
                </div>
              </div>
              <div className="accordion-item" id="question5">
                <a className="accordion-link" href="#question5">
                  Lurer du fortsatt på noe?
                  <Icon
                    className="div-icon-add"
                    icon={chevronDown}
                    size={15}
                  ></Icon>
                  <Icon
                    class="div-icon-remove"
                    icon={chevronUp}
                    size={15}
                  ></Icon>
                </a>
                <div className="awnser">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Integer aliquam sem vitae nibh hendrerit eleifend vitae
                    aliquet ligula. Mauris rutrum elit ac sapien mattis, at
                    imperdiet nunc commodo.
                  </p>
                </div>
              </div>
              <div className="accordion-item" id="question6">
                <a className="accordion-link" href="#question6">
                  How often do you go to the beach?
                  <Icon
                    className="div-icon-add"
                    icon={chevronDown}
                    size={15}
                  ></Icon>
                  <Icon
                    class="div-icon-remove"
                    icon={chevronUp}
                    size={15}
                  ></Icon>
                </a>
                <div className="awnser">
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    Integer aliquam sem vitae nibh hendrerit eleifend vitae
                    aliquet ligula. Mauris rutrum elit ac sapien mattis, at
                    imperdiet nunc commodo.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>
      </Body>
    );
  }
}

export default QA;

const Body = styled.div`
  padding-top: 150px;
  ::before,
  ::after {
    margin: 0;
    padding: 0;
    box-sizing: inherit;
  }

  .mainTitle {
    font-weight: 700;
    font-size: 40px;
    color: #273362;
    line-height: 1.25em;
    text-align: left;
  }

  // Section used for centering process
  .section {
    width: 100%;
    height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
  }

  .qa-container {
    width: 100%;
    max-width: 80rem;
    margin: 0 auto;
    padding: 0 1.5rem;
  }

  .accordion-item {
    margin-bottom: 1rem;
    padding: 1rem;
    box-shadow: 0.5rem 2px 0.5rem rgba(0, 0, 0, 0.1);
  }

  .accordion-link {
    font-size: 1.5rem;
    color: var(--main-dark-blue);
    text-decoration: none;
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: space-between;
    padding: 1rem 0;
  }

  .div-icon-add {
    color: #e7d5ff;
    padding: 0.5rem;
  }
  .accordion-link .div-icon-remove {
    display: none;
  }

  .awnser {
    overflow: hidden;
    max-height: 0;
    position: relative;
    background: #f6f6f6;
    transition: max-height 650ms;
  }

  .awnser:before {
    content: "";
    position: absolute;
    width: 0.6rem;
    height: 90%;
    background-color: #1bcaff;
    top: 50%;
    left: 0;
    transform: translateY(-50%);
  }

  .awnser p {
    font-size: 1.5rem;
    color: #000;
    padding: 2rem;
  }

  .accordion-item:target .awnser {
    max-height: 20rem;
  }
`;
