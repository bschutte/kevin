import React, { Component } from "react";
import { Icon } from "react-icons-kit";
import styled from "styled-components";
import { alignJustify } from "react-icons-kit/fa/alignJustify";
import { close } from "react-icons-kit/fa/close";
import logo from "../svg/testlogo2.svg";

class Navbar2 extends Component {
  render() {
    return (
      <Body>
        <header>
          <div class="menu-container">
            <nav>
              <div class="menu-icons">
                <Icon class="menu-open" icon={alignJustify} size={35}></Icon>
                <Icon class="menu-close" icon={close} size={35}></Icon>
              </div>
              <a href="index.js" class="menu-logo">
                <img src={logo} />
              </a>
              <ul class="nav-list">
                <li>
                  <a href="#">Om oss</a>
                </li>
                <li>
                  <a href="#">Alle kurser</a>
                </li>
                <li class="move-right btn">
                  <a href="#">Min profil</a>
                </li>
              </ul>
            </nav>
          </div>
        </header>
      </Body>
    );
  }
}

export default Navbar2;

const Body = styled.div`
  padding: 0;
  margin: 0;
  overflow: hidden;
  box-sizing: border-box;

  a {
    text-decoration: none;
  }

  ul {
    list-style: none;
  }

  header {
    width: 100%;
    position: absolute;
    top: 0;
    left: 0;
  }

  .menu-container {
    width: 100%;
    max-width: 117rem;
    margin: 0 auto;
    padding: 0 1.5rem;
  }

  .menu-icons {
    color: var(--main-dark-blue);
    font-size: 4rem;
    position: absolute;
    top: 50%;
    right: 2rem;
    transform: translateY(-50%);
    cursor: pointer;
    z-index: 1500;
    display: none;
  }

  nav {
    display: flex;
    align-items: center;
    width: 100%;
    height: 8rem;
    border-bottom: 1px solid rgba(255, 255, 255, 0.1);
  }

  .menu-logo {
    width: 15rem;
    height: 13rem;
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 3rem;
    margin-right: 5rem;
  }

  .nav-list {
    display: flex;
    width: 100%;
    align-items: center;
  }
  .nav-list li {
    line-height: 8rem;
    position: relative;
  }

  .nav-list a {
    display: block;
    color: #fff;
    padding: 0 1.5rem;
    font-size: 1.2rem;
    font-weight: 700;
    text-transform: uppercase;
    transition: color 650ms;
  }

  .nav-list a:hover {
    color: var(--main-dark-blue);
  }

  .btn {
    padding: 1.3rem;
    display: inline-block;
    background-color: var(--main-dark-blue);
    border: 2px solid var(--main-dark-blue);
    border-radius: 5px;
    transition: background-color 650ms;
  }

  .btn:hover {
    color: var(--main-light-blue);
    background-color: rgba(0, 0, 0, 0.2);
  }

  li.move-right {
    margin: auto 0 auto auto;
    line-height: initial;
  }
`;
