import React, { Component } from "react";
import styled from "styled-components";
import logo from "../svg/testlogo2.svg";
import { NavLink, Link } from "react-router-dom";
import { Icon } from "react-icons-kit";
import { alignJustify } from "react-icons-kit/fa/alignJustify";
// Media query
import { generateMedia } from "styled-media-query";

class Navbar extends Component {
  render() {
    return (
      <Body>
        <div className="header-top">
          <div class="menu">
            <NavLink to="/">
              <Logo class="menu-logo" src={logo} />
            </NavLink>
            <NavLink to="/login" className="btn signIn-btn">
              Logg inn
            </NavLink>
            <NavLink to="/om-oss" className="basic-btn">
              OM OSS{" "}
            </NavLink>
            <NavLink to="/login" className="basic-btn1">
              ALLE KURSER
            </NavLink>
          </div>
        </div>
      </Body>
    );
  }
}

export default Navbar;

const customMedia = generateMedia({
  largeDesktop: "1350px",
  medDesktop: "1150px",
  tablet: "960px",
  smallTablet: "740px",
  mobile: "46em"
});

//Logo
const Logo = styled.img`
  width: 15rem;
  height: 12rem;
  position: absolute;
  top: 5%;
  padding-top: 25px;
  left: 50%;
  transform: translate(-50%, -50%);
  margin-left: 0;
  ${customMedia.lessThan("tablet")`
        left: 20%;
    `}
`;

const Body = styled.div`
  padding-top: 7px;
  background: transparent;


  // Header Top
  .header-top {
    height: 10rem;
  }

  .basic-btn{ 
    right: 0;
    color: #273362;
    margin: 1.25rem 9% 0;
    padding: 0.4375rem 1.0625rem;
    font-weight: 700;
    line-height: normal:
    font-size: 1rem;
    position: absolute;
    translate: transform (-50%,-50%);
    cursor: pointer;
    transition: background 0.2 ease-in;
    &:hover {
        color: #4C68FF;
        text-decoration: none;
    }
    ${customMedia.lessThan("smallTablet")`
    margin-top: 1.25rem;
    right: 5%;
    `}
    ${customMedia.greaterThan("tablet")`
    padding: 0.4375rem 3rem;
  `}
}

.basic-btn1 {
    right: 0;
    color: #273362;
    margin: 1.25rem 15% 0;
    padding: 0.4375rem 2rem;
    font-weight: 700;
    line-height: normal:
    font-size: 1rem;
    position: absolute;
    translate: transform (-50%,-50%);
    cursor: pointer;
    transition: background 0.2 ease-in;
    &:hover {
        color: #4C68FF;
        text-decoration: none;
    }
    ${customMedia.lessThan("smallTablet")`
    margin-top: 1.25rem;
    right: 5%;
`}
    ${customMedia.greaterThan("tablet")`
    padding: 0.4375rem 2rem;
`}
}

.signIn-btn {
  right: 0;
  margin: 15px 3% 0;
  padding: 11px 25px 13px 25px; 
  font-weight: 700;
  color: #fff;
  line-height: normal;
  border-radius: 0.1875rem;
  font-size: 1rem;
  background: var(--main-blue); 
  position: absolute;
  translate: transform(-50%, -50%);
  cursor: pointer;
  transition: background 0.2s ease-in;
  &:hover {
      background: #4259d5;
      color: rgb(255,255,255,0.8);
  }
  ${customMedia.lessThan("smallTablet")`
      margin-top: 1.25rem;
      right: 5%;
  `}

}


`;
