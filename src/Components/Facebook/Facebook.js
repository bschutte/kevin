import React, { Component } from "react";
import FacebookLogin from "react-facebook-login/dist/facebook-login-render-props";
import { Icon } from "react-icons-kit";
import { facebookSquare } from "react-icons-kit/fa/facebookSquare";
import { google } from "react-icons-kit/fa/google";
import "../../css/LoginForm.css";
import styled from "styled-components";

class Facebook extends Component {
  state = {
    isLoggedIn: false,
    userID: "",
    name: "",
    email: "",
    picture: ""
  };

  responseFacebook = response => {
    // console.log(response);

    this.setState({
      isLoggedIn: true,
      userID: response.userID,
      name: response.name,
      email: response.email,
      picture: response.picture.data.url
    });
  };

  componentClicked = () => console.log("clicked");

  render() {
    let fbContent;

    if (this.state.isLoggedIn) {
      fbContent = (
        <div
          style={{
            width: "400px",
            margin: "auto",
            background: "#f4f4f4",
            color: "#000",
            padding: "20px"
          }}
        >
          <img src={this.state.picture} alt={this.state.name} />
          <h2>Velkommen {this.state.name}</h2>
          Email: {this.state.email}
        </div>
      );
    } else {
      fbContent = (
        <FacebookLogin
          appId="1163646300509049"
          autoLoad={true}
          fields="name,email,picture"
          onClick={this.componentClicked}
          callback={this.responseFacebook}
          render={renderProps => (
            <Icon
              class="fbIcon"
              style={{ cursor: "pointer" }}
              icon={facebookSquare}
              size={40}
              onClick={renderProps.onClick}
            >
              This is my custom FB button
            </Icon>
          )}
        />
      );
    }

    return (
      <Body>
        <div className="login-social-container">
          <div className="login-icons">{fbContent}</div>
          <div className="login-icons">
            <Icon
              className="googleIcon"
              style={{ cursor: "pointer" }}
              icon={google}
              size={40}
            ></Icon>
          </div>
        </div>
      </Body>
    );
  }
}

export default Facebook;

const Body = styled.div`
  padding: 0;
  margin: 0;

  .login-social-container {
    display: flex;
    flex-direction: row;
  }

  .login-icons {
    padding: 0 10px;
  }
`;
