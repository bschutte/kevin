import React, { Component } from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
// Media query
import { generateMedia } from "styled-media-query";

class FeaturesTab extends Component {
  render() {
    return (
      <Body className="feature-container">
        <div className="container-features">
          <div className="card">
            <div>
              <h2>
                Mattekurs online
                <p>
                  Lorem Ipsum is simply dummy text of the printing and
                  typesetting industry. Lorem Ipsum has been the industry's
                  standard dummy text ever since the 1500s, when an unknown
                  printer took a galley of type and scrambled it to make a type
                  specimen book.
                </p>
                <Link className="main-link" to="#">
                  Read more
                </Link>
              </h2>
            </div>
          </div>

          <div className="card">
            <div>
              <h2>
                Alle trinn
                <p>
                  Lorem Ipsum is simply dummy text of the printing and
                  typesetting industry. Lorem Ipsum has been the industry's
                  standard dummy text ever since the 1500s, when an unknown
                  printer took a galley of type and scrambled it to make a type
                  specimen book.
                </p>
                <Link className="main-link" to="#">
                  Read more
                </Link>
              </h2>
            </div>
          </div>

          <div className="card">
            <div>
              <h2>
                Design
                <p>
                  Lorem Ipsum is simply dummy text of the printing and
                  typesetting industry. Lorem Ipsum has been the industry's
                  standard dummy text ever since the 1500s, when an unknown
                  printer took a galley of type and scrambled it to make a type
                  specimen book.
                </p>
                <Link className="main-link" to="#">
                  Read more
                </Link>
              </h2>
            </div>
          </div>
        </div>
      </Body>
    );
  }
}

export default FeaturesTab;

const customMedia = generateMedia({
  largeDesktop: "1350px",
  medDesktop: "1150px",
  tablet: "960px",
  smallTablet: "740px",
  mobile: "46em"
});

const Body = styled.div`
  margin: 0;
  padding: 0;
  box-sizing: border-box;
  display: flex;
  justify-content: center;
  align-items: center;
  min-height: 60vh;
  ${customMedia.greaterThan("tablet")`
  min-height: 90vh;
  ${customMedia.greaterThan("largeDesktop")`
  min-height: 75vh;
  `}
`}

  // Container
  .container-features {
    width: 1050px;
    display: flex;
    justify-content: space-between;
  }

  // Card
  .card {
    position: relative;
    width: 320px;
    padding: 40px;
    box-shadow: 0 10px 30px rgba(0, 0, 0, 0.1);
    overflow: hidden;
  }

  .card:before {
    content: "";
    position: absolute;
    left: 0;
    bottom: calc(-100% + 4px);
    width: 100%;
    height: 100%;
    background: #000;
    z-index: 1;
    transition: 0.5s ease-in-out;
  }

  // Card Hover

  .card:hover:before {
    bottom: 0;
  }
  // Card Before Hover

  .card: nth-child(1):before {
    background: #ff56ac;
  }
  .card:nth-child(2):before {
    background: #1bcaff;
  }
  .card:nth-child(3):before {
    background: #e33cff;
  }

  // Card Div
  .card div {
    position: relative;
    z-index: 2;
  }

  // Card h2
  .card h2 {
    margin-bottom: 10px;
    font-size: 30px;
    transition: 0.5s;
    color: var(--main-dark-blue);
  }

  // Card p
  .card p {
    font-size: 18px;
    transition: 0.5s;
    color: var(--main-dark-blue);
  }

  // main-link
  .main-link {
    margin-top: 20px;
    font-size: 14px;
    display: inline-block;
    text-decoration: none;
    transition: 0.5s;
    background: #fff;
    color: var(--main-dark-blue);
    padding: 6px 10px;
    font-weight: 600;
    box-shadow: 0 2px 20px rgba(0, 0, 0, 0.05);
  }

  // h2 and p hover
  .card:hover h2,
  .card:hover p {
    color: #fff;
  }
`;
