import React, { Component } from "react";
import styled from "styled-components";
import { Icon } from "react-icons-kit";
import { graduationCap } from "react-icons-kit/fa/graduationCap";
import { check } from "react-icons-kit/fa/check";
import { close } from "react-icons-kit/fa/close";
import { NavLink, Link } from "react-router-dom";
import { generateMedia } from "styled-media-query";

/* import Stripepayment from "../pages/stripepayment"; */

class PriceTable extends Component {
  render() {
    return (
      <Body>
        <section className="section">
          <div className="container-fluid">
            <div className="price-container">
              <div className="row">
                <div className="col-sm-3">
                  <div className="card text-center">
                    <div className="title">
                      <Icon className="Icon" icon={graduationCap} size={60} />
                      <h2>Grunnskole</h2>
                    </div>
                    <div className="price">
                      <h4>
                        <sup>Kr</sup>99,-
                      </h4>
                      <div className="Option">
                        <ul>
                          <li className="bulletPoints">
                            <Icon icon={check} size={15} aria-hidden="true" />
                            Alle trinn
                          </li>
                          <li className="bulletPoints">
                            <Icon icon={check} size={15} aria-hidden="true" />
                            Videoer
                          </li>
                          <li className="bulletPoints">
                            <Icon icon={check} size={15} aria-hidden="true" />
                            Quizoppgaver
                          </li>
                          <li className="bulletPoints">
                            <Icon icon={check} size={15} aria-hidden="true" />
                            Kompendium
                          </li>
                        </ul>
                      </div>
                      <Link className="link-btn" to="/">
                        Kjøp nå
                      </Link>
                    </div>
                  </div>
                </div>
                <div className="col-sm-3">
                  <div className="card text-center">
                    <div className="title">
                      <Icon className="Icon" icon={graduationCap} size={60} />
                      <h2>Ungdomsskole</h2>
                    </div>
                    <div className="price">
                      <h4>
                        <sup>Kr</sup>129,-
                      </h4>
                      <div className="Option">
                        <ul>
                          <li className="bulletPoints">
                            <Icon icon={check} size={15} aria-hidden="true" />
                            Videoer
                          </li>
                          <li className="bulletPoints">
                            <Icon icon={check} size={15} aria-hidden="true" />
                            Quizoppgaver
                          </li>
                          <li className="bulletPoints">
                            <Icon icon={check} size={15} aria-hidden="true" />
                            Kompendium
                          </li>
                          <li className="bulletPoints">
                            <Icon icon={check} size={15} aria-hidden="true" />
                            Alle trinn
                          </li>
                        </ul>
                      </div>
                      <Link className="link-btn" to="/">
                        Kjøp nå
                      </Link>
                    </div>
                  </div>
                </div>
                <div className="col-sm-3">
                  <div className="card text-center">
                    <div className="title">
                      <Icon className="Icon" icon={graduationCap} size={60} />
                      <h2>Videregående</h2>
                    </div>
                    <div className="price">
                      <h4>
                        <sup>Kr</sup>149,-
                      </h4>
                      <div className="Option">
                        <ul>
                          <li className="bulletPoints">
                            <Icon icon={check} size={15} aria-hidden="true" />
                            P-Matte
                          </li>
                          <li className="bulletPoints">
                            <Icon icon={check} size={15} aria-hidden="true" />
                            S1-Matte
                          </li>
                          <li className="bulletPoints">
                            <Icon icon={check} size={15} aria-hidden="true" />
                            S2-Matte
                          </li>
                          <li className="bulletPoints">
                            <Icon icon={check} size={15} aria-hidden="true" />
                            R1 & R2
                          </li>
                        </ul>
                      </div>
                      <Link className="link-btn" to="/">
                        Kjøp nå
                      </Link>
                    </div>
                  </div>
                </div>
                <div className="col-sm-3">
                  <div className="card text-center">
                    <div className="title">
                      <Icon className="Icon" icon={graduationCap} size={60} />
                      <h2>Universitet</h2>
                    </div>
                    <div className="price">
                      <h4>
                        <sup>Kr</sup>249,-
                      </h4>
                      <div className="Option">
                        <ul>
                          <li className="bulletPoints">
                            <Icon icon={check} size={15} aria-hidden="true" />
                            Matte1000
                          </li>
                          <li className="bulletPoints">
                            <Icon icon={check} size={15} aria-hidden="true" />
                            Matte200
                          </li>
                          <li className="bulletPoints">
                            <Icon icon={check} size={15} aria-hidden="true" />
                            Økonomi
                          </li>
                          <li className="bulletPoints">
                            <Icon icon={close} size={15} aria-hidden="true" />
                            Statistikk
                          </li>
                        </ul>
                      </div>
                      <Link className="link-btn" to="/">
                        Kjøp nå
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </Body>
    );
  }
}

export default PriceTable;

const customMedia = generateMedia({
  largeDesktop: "1350px",
  medDesktop: "1150px",
  tablet: "960px",
  smallTablet: "740px",
  mobile: "46em"
});

const Body = styled.div`
  margin: 0;
  padding: 0 0 100px 0;
  font-family: "Roboto", sans-serif !important;
  ${customMedia.greaterThan("tablet")`
  margin: -5% 6.5%;`}

  // Section
  .section {
    width: 100%;
    height: 100vh;
    box-sizing: border-box;
    padding: 100px 0;
  }


  // card
  .card {
    position: relative;
    max-width: 300px;
    height: auto;
    border-radius: 15px;
    margin: 0 auto;
    padding: 40px 20px;
    box-shadow: 0 10px 15px rgba(0, 0, 0, 0.4);
    transition: 0.5s;
    overflow: hidden;
    }

    .card:hover {
        transform: scale(1.1);
    }

  //Col-sm-3 .1stcard
  .col-sm-3:nth-child(1) .card,
  .col-sm-3:nth-child(1) .card .title .Icon {
    background: linear-gradient(-45deg, #f403d1, #64b5f6);
  }
  //Col-sm-3 .2 ndcard
  .col-sm-3:nth-child(2) .card,
  .col-sm-3:nth-child(2) .card .title .Icon {
    background: linear-gradient(-45deg, #ffec61, #f321d7);
  }
  //Col-sm-3 .3ndcard
  .col-sm-3:nth-child(3) .card,
  .col-sm-3:nth-child(3) .card .title .Icon {
    background: linear-gradient(-45deg, #24ff72, #9a4eff);
  }
  //Col-sm-3 .4th card
  .col-sm-3:nth-child(4) .card,
  .col-sm-3:nth-child(4) .card .title .Icon {
    background: linear-gradient(-45deg, var(--main-blue), #64b5f6);
  }

  //Card Before (White grading)
  .card:before {
    content: "";
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 40%;
    background: rgba(255, 255, 255, 0.1);
    z-index: 1;
    transform: skewY(-5deg) scale(1.5);
  }

  // Icons
  .title .Icon {
    color: #fff;
    font-size: 60px;
    width: 100px;
    height: 100px;
    border-radius: 50%;
    text-align: center;
    line-height: 100px;
    box-shadow: 0 10px 10px rgba(0, 0, 0, 0.3);
  }

  //H2 title
  .title h2 {
    position: relative;
    margin: 20px 0 0;
    padding: 0;
    color: #fff;
    font-size: 28px;
    z-index: 2;
  }

  //The price
  .price {
    position: relative;
    z-index: 2;
  }

  .price h4 {
    margin: 0;
    padding: 20px 0;
    color: #fff;
    font-size: 60px;
  }

  // Options
  .option {
    position: relative;
    z-index: 2;
  }

  .option ul {
    margin: 0;
    padding: 0;
  }

  //Bullet points
  .bulletPoints {
    margin: 0 55px 10px 0px;
    padding: 0;
    list-style: none;
    color: #fff;
    font-size: 16px;
    cursor-pointer: none;
  }

  // Link
  .link-btn {
    position: relative;
    z-index: 2;
    background: #fff;
    color: #262626;
    width: 150px;
    height: 40px;
    line-height: 40px;
    border-radius: 40px;
    display: block;
    text-align: center;
    margin 20px auto 0;
    font-size: 16px;
    cursor: pointer;
    text-decoration: none;
    box-shadow: 0 5px 10px rgba(0,0,0,0.2);
  }
  .link-btn:hover {
      text-
  }
`;
