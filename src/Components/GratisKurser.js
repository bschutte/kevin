import React, { Component } from "react";
import { Button } from "./Buttons/Button";
import styled from "styled-components";
// Icons
import { Icon } from "react-icons-kit";
import { desktop } from "react-icons-kit/fa/desktop";
import { pencil } from "react-icons-kit/fa/pencil";

// Link
import { Link, NavLink } from "react-router-dom";

// Media query
import { generateMedia } from "styled-media-query";

// All Courses import

class TabContentTwo extends Component {
  render() {
    return (
      <div>
        <midSection className="mid-section-kurser">
          <div className="mid-section-container">
            <div className="mid-section-content">
              <SubtitleOffer>
                Få alle våre kurser tilgjengelig idag og benytt dem hvor og når
                som helst, kun fra{" "}
                <a style={{ color: "var(--main-light-blue" }}>kr 99,-/måned</a>{" "}
              </SubtitleOffer>
              <NavLink to="/priser">
                <ButtonOffer>BESTILL</ButtonOffer>
              </NavLink>
            </div>
            <br></br>
          </div>
        </midSection>
        <Body>
          <div class="card-container">
            <div class="top">
              <h3>Grunnleggende matematikk</h3>
              <div class="icon-container">
                <span class="icon">
                  <Icon icon={desktop} size={15} aria-hidden="true" /> 1t 23min
                </span>
                <span class="icon">
                  <Icon icon={pencil} size={15} aria-hidden="true" />
                  123 quizoppgaver
                </span>
              </div>
            </div>
            <div class="bottom">
              <a href="" class="fake-button">
                Kom i gang
              </a>
            </div>
          </div>
          <div class="card-container">
            <div class="top">
              <h3>Card title</h3>
              <div class="icon-container">
                <span class="icon">
                  <Icon icon={desktop} size={15} aria-hidden="true" /> 1t 23min
                </span>
                <span class="icon">
                  <Icon icon={pencil} size={15} aria-hidden="true" /> 123
                  quizoppgaver
                </span>
              </div>
            </div>
            <div class="bottom">
              <a href="" class="fake-button">
                Kom i gang
              </a>
            </div>
          </div>
          <div class="card-container">
            <div class="top">
              <h3>Card title</h3>
              <div class="icon-container">
                <span class="icon">
                  <Icon icon={desktop} size={15} aria-hidden="true" /> 1t 23min
                </span>
                <span class="icon">
                  <Icon icon={pencil} size={15} aria-hidden="true" />
                  123 quizoppgaver
                </span>
              </div>
            </div>
            <div class="bottom">
              <a href="" class="fake-button">
                Kom i gang
              </a>
            </div>
          </div>
        </Body>
      </div>
    );
  }
}

export default TabContentTwo;

// Media Query
const customMedia = generateMedia({
  largeDesktop: "1350px",
  medDesktop: "1150px",
  tablet: "960px",
  smallTablet: "740px",
  mobile: "46em"
});

// Main Tab Content Container
const Body = styled.div`
  display: flex;
  flex-display: row;
  padding: 45px 0px 35px;
  justify-content: center;
  width: 100%;
  padding: 150px
  background: #1BCAFF;

  //Card container
  .card-container {
    border-radius: 0.5rem;
    box-shadow: 0px 0px 69px -21px rgba(0, 0, 0, 0.45);
    height: 15rem;
    width: 25rem;
    display: flex;
    flex-direction: column;
    margin: 0px 15px 0px 15px;
  }

  .card-container:nth-child(1) {
    background: linear-gradient(-45deg, #f403d1, #64b5f6)
  }
  .card-container:nth-child(2) {
    background: linear-gradient(-45deg, #24ff72, #9a4eff);
  }

  .card-container:nth-child(3) {

    background: linear-gradient(-45deg, var(--main-blue), #64b5f6);
  }

  // Top row
  .top {
    border-radius: 0.25rem 0.25rem 0 0;
    padding: 1.85rem;
  }



  .top h3 {
    color: #fff;
    font-size: 20px;
  }
  // Icon container
  .icon-container {
    padding-top: 10px;
    color: rgba(255, 255, 255, 0.7);
    font-size: 14px;
  }
  .icon:nth-child(2) {
    padding: 0px 0px 0px 45px;
  }

  // Bottom row
  .bottom {
    padding: 1rem 0 1rem 0;
    display: flex;
    justify-content: center;
    align-items: center;
    background: #fff;
    border-radius: 5rem 1.5rem 5rem 1.5rem;
  }

  // Button
  .fake-button {
    border-radius: 0.25rem;
    padding: 1.25rem 7rem;
    text-decoration: none;
    color: #273362;
    font-size: 15px;
    font-weight: bold;
    text-transform: uppercase;
  }

`;

// Subtitle
const SubtitleOffer = styled.h3`
  font-weight: 700;
  font-size: 40px;
  color: #273362;
  padding: 10rem 30rem 0px 30rem;
  line-height: 1.25em;
  text-align: center;
  margin: 0 0 1.875;
  ${customMedia.lessThan("smallTablet")`
    padding-top: 2rem;
    padding: 6rem;
    `}
  ${customMedia.greaterThan("tablet")`
    padding: 75px 200px 0px 200px;
  `}
  ${customMedia.greaterThan("largeDesktop")`
  padding: 2.5rem 25rem 2.5rem 25rem;
`}
`;

const midSection = styled.div`
  .mid-section-container {
    margin: 0 15%;
  }

  // Tab Top Container
  .mid-section-content {
    justify-content: center;
    align-items: center;
    text-align: center;
    padding: 2.5rem 0;
    ${customMedia.lessThan("smallDesktop")`
            grid-template-columns: repeat(2, 1fr);
        `}

    ${customMedia.lessThan("tablet")`
            grid-template-columns: 1fr;
            text-align: center;
            row-gap: 1.5rem;
        `}
  }
`;

const ButtonOffer = styled.button`
  color: #fff;
  background: var(--main-pink);
  outline: none;
  border: none;
  border-radius: 0.5rem;
  padding: 1.25rem 5.5rem 1.25rem 5.5rem;
  font-size: 20px;
  font-weight: 700;
  text-align: center;
  box-shadow: 5px 5px 5px 5px rgba(0, 0, 0, 0.05);
  transition: opacity 0.2s ease-in;
  cursor: pointer;
  text-decoration: none;
  margin: 0px 0px 200px 42.5%;
  &hover: {
    transform: scale(1.1);
    transition: 0.5s;
    top: -70px;
    background: #35db99;
  }
  ${customMedia.greaterThan("tablet")`
  padding: 1.25rem 5.5rem 1.25rem 5.5rem;
  ${customMedia.lessThan("tablet")`
  grid-column: 1 / -1;
  margin-left: 30%;
  margin-right: 30%;
`}
`}
`;
