import React, { Component } from "react";

export const Kurs0 = {
  navn: " Grunnleggende Matematikk",
  tid: "32min",
  oppgaver: "42 quizoppgaver",
  beskrivelse: "grunnleggende matematikk"
};

export const Kurs1 = {
  navn: "Matematikk 1.Klasse",
  tid: "1t 19min",
  oppgaver: "64 quizoppgaver",
  beskrivelse: " grunnleggende matematikk"
};

export const Kurs2 = {
  navn: "Matematikk 2.Klasse",
  tid: "1t 32min",
  oppgaver: "72 quizoppgaver",
  beskrivelse: "grunnleggende matematikk"
};
export const Kurs3 = {
  navn: "Matematikk 3.Klasse",
  tid: "1t 32min",
  oppgaver: "73 quizoppgaver",
  beskrivelse: "grunnleggende matematikk"
};
export const Kurs4 = {
  navn: "Matematikk 4.Klasse",
  tid: "1t 32min",
  oppgaver: "80 quizoppgaver",
  beskrivelse: "grunnleggende matematikk"
};
export const Kurs5 = {
  navn: "Matematikk 5.Klasse",
  tid: "1t 32min",
  oppgaver: "64 quizoppgaver",
  beskrivelse: "grunnleggende matematikk"
};
export const Kurs6 = {
  navn: "Matematikk 6.Klasse",
  tid: "1t 32min",
  oppgaver: "64 quizoppgaver",
  beskrivelse: "grunnleggende matematikk"
};
export const Kurs7 = {
  navn: "Matematikk 7.Klasse",
  tid: "1t 32min",
  oppgaver: "64 quizoppgaver",
  beskrivelse: "grunnleggende matematikk"
};
export const Kurs8 = {
  navn: "Matematikk 8.Klasse",
  tid: "1t 32min",
  oppgaver: "64 quizoppgaver",
  beskrivelse: "grunnleggende matematikk"
};

export const Kurs9 = {
  navn: "Matematikk 9.Klasse",
  tid: "1t 32min",
  oppgaver: "64 quizoppgaver",
  beskrivelse: "grunnleggende matematikk"
};

export const Kurs10 = {
  navn: "Matematikk 10.Klasse",
  tid: "1t 32min",
  oppgaver: "64 quizoppgaver",
  beskrivelse: "grunnleggende matematikk"
};

export const p1 = {
  navn: "Matematikk VGS - 1P",
  tid: "1t 32min",
  oppgaver: "64 quizoppgaver",
  beskrivelse: "grunnleggende matematikk"
};

export const p2 = {
  navn: "Matematikk VGS - 2P",
  tid: "1t 32min",
  oppgaver: "64 quizoppgaver",
  beskrivelse: "grunnleggende matematikk"
};

export const t1 = {
  navn: "Matematikk VGS - 1T",
  tid: "1t 32min",
  oppgaver: "64 quizoppgaver",
  beskrivelse: "grunnleggende matematikk"
};
export const s1 = {
  navn: "Matematikk VGS - S1",
  tid: "1t 32min",
  oppgaver: "64 quizoppgaver",
  beskrivelse: "grunnleggende matematikk"
};
export const s2 = {
  navn: "Matematikk VGS - S2",
  tid: "1t 32min",
  oppgaver: "64 quizoppgaver",
  beskrivelse: "grunnleggende matematikk"
};
export const r1 = {
  navn: "Matematikk VGS - R1",
  tid: "1t 32min",
  oppgaver: "64 quizoppgaver",
  beskrivelse: "grunnleggende matematikk"
};
export const r2 = {
  navn: "Matematikk VGS - R2",
  tid: "1t 32min",
  oppgaver: "64 quizoppgaver",
  beskrivelse: "grunnleggende matematikk"
};

export const MEK1000 = {
  navn: "Uni/Høgskole - MEK1000",
  tid: "1t 32min",
  oppgaver: "64 quizoppgaver",
  beskrivelse: "grunnleggende matematikk"
};

export const MEK2000 = {
  navn: "Uni/Høgskole - MEK2000",
  tid: "1t 32min",
  oppgaver: "64 quizoppgaver",
  beskrivelse: "grunnleggende matematikk"
};

export const økoMatte = {
  navn: "Matematikk for økonomer",
  tid: "1t 32min",
  oppgaver: "64 quizoppgaver",
  beskrivelse: "grunnleggende matematikk"
};
