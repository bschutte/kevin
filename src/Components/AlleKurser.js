import React, { Component } from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";
import { Icon } from "react-icons-kit";
import { desktop } from "react-icons-kit/fa/desktop";
import { pencil } from "react-icons-kit/fa/pencil";

// All Courses
import {
  Kurs0,
  Kurs1,
  Kurs2,
  Kurs3,
  Kurs4,
  Kurs5,
  Kurs6,
  Kurs7,
  Kurs8,
  Kurs9,
  Kurs10,
  p1,
  p2,
  t1,
  s1,
  s2,
  r1,
  r2,
  MEK1000,
  MEK2000,
  økoMatte
} from "./Kurser/alleKurser";

class KurserTest extends Component {
  render() {
    return (
      <Subtitle>
        <span className="mainTitle">Alle kurser</span>
        {/* End widget span */}
        <Body>
          <div class="card-container">
            <div class="top">
              <h3>{Kurs0.navn}</h3>
              <div class="icon-container">
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={desktop}
                    size={15}
                    aria-hidden="true"
                  />
                  {Kurs0.tid}
                </span>
                <span class="icon">
                  <Icon
                    icon={pencil}
                    size={15}
                    aria-hidden="true"
                    style={{ paddingRight: "7px" }}
                  />
                  {Kurs0.oppgaver}
                </span>
              </div>
            </div>
            <div class="bottom">
              <a href="" class="fake-button">
                Kom i gang
              </a>
            </div>
          </div>
          <div class="card-container">
            <div class="top">
              <h3>{Kurs1.navn}</h3>
              <div class="icon-container">
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={desktop}
                    size={15}
                    aria-hidden="true"
                  />{" "}
                  {Kurs1.tid}
                </span>
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={pencil}
                    size={15}
                    aria-hidden="true"
                  />{" "}
                  {Kurs1.oppgaver}
                </span>
              </div>
            </div>
            <div class="bottom">
              <a href="" class="fake-button">
                Kom i gang
              </a>
            </div>
          </div>
          <div class="card-container">
            <div class="top">
              <h3>{Kurs2.navn}</h3>
              <div class="icon-container">
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={desktop}
                    size={15}
                    aria-hidden="true"
                  />{" "}
                  {Kurs2.tid}
                </span>
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={pencil}
                    size={15}
                    aria-hidden="true"
                  />
                  {Kurs2.oppgaver}
                </span>
              </div>
            </div>
            <div class="bottom">
              <a href="" class="fake-button">
                Kom i gang
              </a>
            </div>
          </div>
        </Body>
        <Body>
          <div class="card-container">
            <div class="top">
              <h3>{Kurs3.navn}</h3>
              <div class="icon-container">
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={desktop}
                    size={15}
                    aria-hidden="true"
                  />{" "}
                  {Kurs3.tid}
                </span>
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={pencil}
                    size={15}
                    aria-hidden="true"
                  />
                  {Kurs3.oppgave}
                </span>
              </div>
            </div>
            <div class="bottom">
              <a href="" class="fake-button">
                Kom i gang
              </a>
            </div>
          </div>
          <div class="card-container">
            <div class="top">
              <h3>{Kurs4.navn}</h3>
              <div class="icon-container">
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={desktop}
                    size={15}
                    aria-hidden="true"
                  />{" "}
                  {Kurs4.tid}
                </span>
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={pencil}
                    size={15}
                    aria-hidden="true"
                  />{" "}
                  {Kurs4.oppgave}
                </span>
              </div>
            </div>
            <div class="bottom">
              <a href="" class="fake-button">
                Kom i gang
              </a>
            </div>
          </div>
          <div class="card-container">
            <div class="top">
              <h3>{Kurs5.navn}</h3>
              <div class="icon-container">
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={desktop}
                    size={15}
                    aria-hidden="true"
                  />{" "}
                  {Kurs5.tid}
                </span>
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={pencil}
                    size={15}
                    aria-hidden="true"
                  />
                  {Kurs5.oppgaver}
                </span>
              </div>
            </div>
            <div class="bottom">
              <a href="" class="fake-button">
                Kom i gang
              </a>
            </div>
          </div>
        </Body>
        <Body>
          <div class="card-container">
            <div class="top">
              <h3>{Kurs6.navn}</h3>
              <div class="icon-container">
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={desktop}
                    size={15}
                    aria-hidden="true"
                  />{" "}
                  {Kurs6.tid}
                </span>
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={pencil}
                    size={15}
                    aria-hidden="true"
                  />
                  {Kurs6.oppgaver}
                </span>
              </div>
            </div>
            <div class="bottom">
              <a href="" class="fake-button">
                Kom i gang
              </a>
            </div>
          </div>
          <div class="card-container">
            <div class="top">
              <h3>{Kurs7.navn}</h3>
              <div class="icon-container">
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={desktop}
                    size={15}
                    aria-hidden="true"
                  />{" "}
                  {Kurs7.tid}
                </span>
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={pencil}
                    size={15}
                    aria-hidden="true"
                  />{" "}
                  {Kurs7.oppgaver}
                </span>
              </div>
            </div>
            <div class="bottom">
              <a href="" class="fake-button">
                Kom i gang
              </a>
            </div>
          </div>
          <div class="card-container">
            <div class="top-ungdom">
              <h3>{Kurs8.navn}</h3>
              <div class="icon-container">
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={desktop}
                    size={15}
                    aria-hidden="true"
                  />{" "}
                  {Kurs8.tid}
                </span>
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={pencil}
                    size={15}
                    aria-hidden="true"
                  />
                  {Kurs8.oppgaver}
                </span>
              </div>
            </div>
            <div class="bottom">
              <a href="" class="button-ungdom">
                Kom i gang
              </a>
            </div>
          </div>
        </Body>
        <Body>
          <div class="card-container">
            <div class="top-ungdom">
              <h3>{Kurs9.navn}</h3>
              <div class="icon-container">
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={desktop}
                    size={15}
                    aria-hidden="true"
                  />{" "}
                  {Kurs9.tid}
                </span>
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={pencil}
                    size={15}
                    aria-hidden="true"
                  />
                  {Kurs9.oppgaver}
                </span>
              </div>
            </div>
            <div class="bottom">
              <a href="" class="button-ungdom">
                Kom i gang
              </a>
            </div>
          </div>
          <div class="card-container">
            <div class="top-ungdom">
              <h3>{Kurs10.navn}</h3>
              <div class="icon-container">
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={desktop}
                    size={15}
                    aria-hidden="true"
                  />{" "}
                  {Kurs10.tid}
                </span>
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={pencil}
                    size={15}
                    aria-hidden="true"
                  />{" "}
                  {Kurs10.oppgaver}
                </span>
              </div>
            </div>
            <div class="bottom">
              <a href="" class="button-ungdom">
                Kom i gang
              </a>
            </div>
          </div>
          <div class="card-container">
            <div class="top-vgs">
              <h3>{p1.navn}</h3>
              <div class="icon-container">
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={desktop}
                    size={15}
                    aria-hidden="true"
                  />{" "}
                  {p1.tid}
                </span>
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={pencil}
                    size={15}
                    aria-hidden="true"
                  />
                  {p1.oppgaver}
                </span>
              </div>
            </div>
            <div class="bottom">
              <a href="" class="button-vgs">
                Kom i gang
              </a>
            </div>
          </div>
        </Body>
        <Body>
          <div class="card-container">
            <div class="top-vgs">
              <h3>{p2.navn}</h3>
              <div class="icon-container">
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={desktop}
                    size={15}
                    aria-hidden="true"
                  />{" "}
                  {p2.tid}
                </span>
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={pencil}
                    size={15}
                    aria-hidden="true"
                  />
                  {p2.oppgaver}
                </span>
              </div>
            </div>
            <div class="bottom">
              <a href="" class="button-vgs">
                Kom i gang
              </a>
            </div>
          </div>
          <div class="card-container">
            <div class="top-vgs">
              <h3>{t1.navn}</h3>
              <div class="icon-container">
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={desktop}
                    size={15}
                    aria-hidden="true"
                  />{" "}
                  {t1.tid}
                </span>
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={pencil}
                    size={15}
                    aria-hidden="true"
                  />{" "}
                  {t1.oppgaver}
                </span>
              </div>
            </div>
            <div class="bottom">
              <a href="" class="button-vgs">
                Kom i gang
              </a>
            </div>
          </div>
          <div class="card-container">
            <div class="top-vgs">
              <h3>{p2.navn}</h3>
              <div class="icon-container">
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={desktop}
                    size={15}
                    aria-hidden="true"
                  />{" "}
                  {p2.tid}
                </span>
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={pencil}
                    size={15}
                    aria-hidden="true"
                  />
                  {p2.oppgaver}
                </span>
              </div>
            </div>
            <div class="bottom">
              <a href="" class="button-vgs">
                Kom i gang
              </a>
            </div>
          </div>
        </Body>
        <Body>
          <div class="card-container">
            <div class="top-vgs">
              <h3>{s1.navn}</h3>
              <div class="icon-container">
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={desktop}
                    size={15}
                    aria-hidden="true"
                  />{" "}
                  {s1.tid}
                </span>
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={pencil}
                    size={15}
                    aria-hidden="true"
                  />
                  {s1.oppgaver}
                </span>
              </div>
            </div>
            <div class="bottom">
              <a href="" class="button-vgs">
                Kom i gang
              </a>
            </div>
          </div>
          <div class="card-container">
            <div class="top-vgs">
              <h3>{s2.navn}</h3>
              <div class="icon-container">
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={desktop}
                    size={15}
                    aria-hidden="true"
                  />{" "}
                  {s2.tid}
                </span>
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={pencil}
                    size={15}
                    aria-hidden="true"
                  />{" "}
                  {s2.oppgaver}
                </span>
              </div>
            </div>
            <div class="bottom">
              <a href="" class="button-vgs">
                Kom i gang
              </a>
            </div>
          </div>
          <div class="card-container">
            <div class="top-vgs">
              <h3>{r1.navn}</h3>
              <div class="icon-container">
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={desktop}
                    size={15}
                    aria-hidden="true"
                  />{" "}
                  {s2.tid}
                </span>
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={pencil}
                    size={15}
                    aria-hidden="true"
                  />
                  {r1.oppgaver}
                </span>
              </div>
            </div>
            <div class="bottom">
              <a href="" class="button-vgs">
                Kom i gang
              </a>
            </div>
          </div>
        </Body>
        <Body>
          <div class="card-container">
            <div class="top-vgs">
              <h3>{r2.navn}</h3>
              <div class="icon-container">
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={desktop}
                    size={15}
                    aria-hidden="true"
                  />{" "}
                  {r2.tid}
                </span>
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={pencil}
                    size={15}
                    aria-hidden="true"
                  />
                  {r2.oppgaver}
                </span>
              </div>
            </div>
            <div class="bottom">
              <a href="" class="button-vgs">
                Kom i gang
              </a>
            </div>
          </div>
          <div class="card-container">
            <div class="top-uni">
              <h3>{MEK1000.navn}</h3>
              <div class="icon-container">
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={desktop}
                    size={15}
                    aria-hidden="true"
                  />{" "}
                  {MEK1000.tid}
                </span>
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={pencil}
                    size={15}
                    aria-hidden="true"
                  />{" "}
                  {MEK1000.oppgaver}
                </span>
              </div>
            </div>
            <div class="bottom">
              <a href="" class="button-uni">
                Kom i gang
              </a>
            </div>
          </div>
          <div class="card-container">
            <div class="top-uni">
              <h3>{MEK2000.navn}</h3>
              <div class="icon-container">
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={desktop}
                    size={15}
                    aria-hidden="true"
                  />{" "}
                  {MEK2000.tid}
                </span>
                <span class="icon">
                  <Icon
                    style={{ paddingRight: "7px" }}
                    icon={pencil}
                    size={15}
                    aria-hidden="true"
                  />
                  {MEK2000.oppgaver}
                </span>
              </div>
            </div>
            <div class="bottom">
              <a href="" class="button-uni">
                Kom i gang
              </a>
            </div>
          </div>
        </Body>
      </Subtitle>
    );
  }
}

export default KurserTest;

const Subtitle = styled.h3`
  padding-top: 5%;

  .mainTitle {
    font-weight: 700;
    font-size: 40px;
    color: #273362;
    line-height: 1.25em;
    text-align: left;
    padding: 0 0 0 17%;
    margin: 0 0 1.875 0;
  }
`;

const Body = styled.div`
  display: flex;
  flex-display: row;
  padding: 45px 0px 15px;
  justify-content: center;
  width: 100%;

  //Card container grunnskole
  .card-container {
    border-radius: 0.5rem;
    box-shadow: 0px 0px 69px -21px rgba(0, 0, 0, 0.45);
    height: 15rem;
    width: 25rem;
    display: flex;
    flex-direction: column;
    margin: 0px 15px 0px 15px;
  }

  .top h3 {
    color: #fff;
    font-size: 20px;
  }
  // Top row grunnskole
  .top {
    background: linear-gradient(-45deg, #ffec61, #f321d7);
    border-radius: 0.25rem 0.25rem 0 0;
    padding: 1.85rem;
  }

  // Top row ungdom
  .top-ungdom {
    background: linear-gradient(-45deg, #24ff72, #9a4eff);
    border-radius: 0.25rem 0.25rem 0 0;
    padding: 1.85rem;
  }

  .top-ungdom h3 {
    color: #fff;
    font-size: 20px;
  }

  // Top Row VGS
  .top-vgs {
    background: linear-gradient(-45deg, #f403d1, #64b5f6);
    border-radius: 0.25rem 0.25rem 0 0;
    padding: 1.85rem;
  }

  .top-vgs h3 {
    color: #fff;
    font-size: 20px;
  }

  // Top Row Uni
  .top-uni {
    background: linear-gradient(-45deg, var(--main-blue), #64b5f6);
    border-radius: 0.25rem 0.25rem 0 0;
    padding: 1.85rem;
  }

  .top-uni h3 {
    color: #fff;
    font-size: 20px;
  }

  // Icon container
  .icon-container {
    padding-top: 10px;
    color: rgba(255, 255, 255, 0.7);
    font-size: 14px;
  }
  .icon:nth-child(2) {
    padding: 0px 0px 0px 45px;
  }

  // Bottom row
  .bottom {
    padding: 1.25rem 0 0 0;
    display: flex;
    justify-content: center;
    align-items: center;
  }

  // Button
  .fake-button {
    background: linear-gradient(-45deg, #ffec61, #f321d7);
    border-radius: 0.25rem;
    padding: 1.25rem 7rem;
    text-decoration: none;
    color: #fff;
    font-size: 12px;
    text-transform: uppercase;
  }

  // Button ungdom
  .button-ungdom {
    background: linear-gradient(-45deg, #24ff72, #9a4eff);
    border-radius: 0.25rem;
    padding: 1.25rem 7rem;
    text-decoration: none;
    color: #fff;
    font-size: 12px;
    text-transform: uppercase;
  }

  // Button Vgs
  .button-vgs {
    background: linear-gradient(-45deg, #f403d1, #64b5f6);
    border-radius: 0.25rem;
    padding: 1.25rem 7rem;
    text-decoration: none;
    color: #fff;
    font-size: 12px;
    text-transform: uppercase;
  }

  // Button Universitet
  .button-uni {
    background: linear-gradient(-45deg, var(--main-blue), #64b5f6);
    border-radius: 0.25rem;
    padding: 1.25rem 7rem;
    text-decoration: none;
    color: #fff;
    font-size: 12px;
  }
`;
