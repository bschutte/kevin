import React, { Component } from "react";
import { Button } from "./Buttons/offerbutton";
import { Link } from "react-router-dom";
import styled from "styled-components";
import { Icon } from "react-icons-kit";
import { ic_keyboard_arrow_right } from "react-icons-kit/md/ic_keyboard_arrow_right";

class Footer extends Component {
  render() {
    return (
      <FooterContainer>
        <div className="footer-container">
          <span
            style={{
              marginLeft: "37.5%",
              fontSize: "2.125rem",
              fontWeight: "700",
              color: "#000"
            }}
          >
            Enda ikke overbevist?
            {/* Prøv Nå Knapp */}
            <button className="try-btn">
              Prøv oss <Icon icon={ic_keyboard_arrow_right} size={20} />{" "}
            </button>
          </span>
          <div className="footer-columns">
            <ul>
              <li>
                <Link>AMatte </Link>
                <span>
                  <Link>Brukervilkår </Link>
                </span>
                <Link>Personvern </Link>
                <Link>Cookies </Link>
                <Link>Copyright @AMatte - All Rights Reserved</Link>
              </li>
            </ul>
          </div>
        </div>
      </FooterContainer>
    );
  }
}

export default Footer;

const FooterContainer = styled.footer`
    background: var(--main-white);
    padding-top: 10rem;
    padding-bottom: 3rem;
    color: #273362;

    .footer-columns {
        width: 70%;
        margin: 1rem auto 0;
        font-size: 0.9rem;
        overflow: auto;
        padding-top: 15%;
        justify-content: center;
        text-align: center;
        display: flex;
    }

    ul li {
        list-style: none;
        line-height: 2.5;
    }
    
    a {
        color: #000;
    }

    a:hover {
        text-decoration: underline;
        color: #999;
        cursor: pointer;
    }

    // Prøv Nå Knapp
    .try-btn {
        color: #fff;
        background: #35DB99;
        outline: none;
        border: none;
        border-radius: 0.5rem;
        padding: 1.25rem 2.50rem 1.25rem 2.50rem;
        font-size: 20px;
        font-weight: 700;
        text-align: center;
        box-shadow: 5px 5px 0 rgba(0,0,0,0.45);
        transition: opacity .2s ease-in;
        cursor: pointer;
        text-decoration: none;
        margin: 0 15px;
        &hover: {
            top: -70px;
            background: #35DB99;
    }

`;
