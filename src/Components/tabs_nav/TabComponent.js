import React, { Component } from "react";
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import TabDoor from "./TabDoor";
import TabDevice from "./TabDevice";
// Tabs Content
import TabComponentOne from "./TabComponentOne";
import TabComponentTwo from "./TabContentTwo";
import TabComponentThree from "./TabContentThree";
import TabPrice from "./TabPrice";
import "../../css/TabsNav.css";

class TabComponent extends Component {
  state = {
    tabIndex: 0
  };
  render() {
    return (
      <div>
        <Tabs
          className="tabs"
          selectedIndex={this.state.tabIndex}
          onSelect={tabIndex => this.setState({ tabIndex })}
        >
          <TabList className="tab-nav-container">
            <Tab
              className={`${
                this.state.tabIndex === 0 ? "tab-selected active" : null
              }`}
            >
              <TabDevice />
              <p className="largeScreen" style={{ marginTop: "-5.3125rem" }}>
                <strong>
                  Tilgjengelig overal
                  <br />
                  På mobil, nettbrett og og pc
                </strong>
              </p>
              <span
                className="mediumScreen"
                style={{ marginTop: "-5.3125rem" }}
              >
                Alle skjermer
              </span>
            </Tab>
            <Tab
              className={`${
                this.state.tabIndex === 1 ? "tab-selected active" : null
              }`}
            >
              <TabDoor />
              <p className="largeScreen" style={{ marginBottom: "1.875rem" }}>
                <strong>
                  For alle elever
                  <br />
                </strong>
              </p>
              <br />
              <span className="mediumScreen" style={{ marginTop: "0.4rem" }}>
                Alle elever
              </span>
            </Tab>
            <Tab
              className={`${
                this.state.tabIndex === 2 ? "tab-selected active" : null
              }`}
            >
              <TabPrice />
              <p className="largeScreen" style={{ marginBottom: "1.875rem" }}>
                <strong>
                  Velg din plan
                  <br />
                </strong>
              </p>
              <br />
              <span className="mediumScreen" style={{ marginTop: "0.4rem" }}>
                Priser
              </span>
            </Tab>
          </TabList>
          {/* Tabs Content */}
          <TabPanel>
            <TabComponentOne />
          </TabPanel>
          <TabPanel>
            <TabComponentTwo />
          </TabPanel>
          <TabPanel>
            <TabComponentThree />
          </TabPanel>
        </Tabs>
      </div>
    );
  }
}

export default TabComponent;
