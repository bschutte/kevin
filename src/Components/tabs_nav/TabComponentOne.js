import React from "react";
import Img from "../../svg/laptop_no_screen.svg";
import styled from "styled-components";
import { Button } from "../Buttons/Button";
import { generateMedia } from "styled-media-query";
import { NavLink, Link } from "react-router-dom";

function TabContentOne() {
  return (
    <TabContentContainer>
      <div className="Container">
        <div className="tab-content">
          <div>
            <span className="title" style={{ marginBottom: "2rem" }}>
              Våre kurs laget av toppstudenter innen fagfeltet skal hjelpe deg
              med å oppnå topp karakter via trygg gjennomgang, kvalitetspørsmål
              og test-deg-selv oppgaver.
            </span>
            <br />
            <NavLink to="/priser">
              <Button style={{ marginTop: "2rem" }}>Prøv nå</Button>
            </NavLink>
          </div>
          <div className="video-container">
            <img src={Img} />
            <div className="centered">
              <iframe
                className="visitVideo"
                src="https://fast.wistia.net/embed/iframe/234hdmz4yk"
                title="video1.mp4"
                allowtransparency="true"
                frameborder="0"
                scrolling="no"
                allowfullscreen
                mozallowfullscreen
                webkitallowfullscreen
                oallowfullscreen
                msallowfullscreen
                width="420"
                height="242"
              ></iframe>
              <script
                src="https://fast.wistia.net/assets/external/E-v1.js"
                async
              ></script>
            </div>
          </div>
        </div>
      </div>
    </TabContentContainer>
  );
}

export default TabContentOne;

// Media Query
const customMedia = generateMedia({
  largeDesktop: "1350px",
  medDesktop: "1150px",
  tablet: "960px",
  smallTablet: "740px",
  mobile: "46em"
});

// Main Content Container
const TabContentContainer = styled.div`
  background: #e5e5e5;
  padding: 0px 4rem 55px 4rem;
  color: #273362;

  .container {
    margin: 0 10%;
  }

  img {
    width: 100%;
  }

  .title {
    margin-top: 2rem;
    ${customMedia.lessThan("smallDesktop")`
        font-size: 1.5rem;
        line-height: 1;
        `}
  }

  .tab-content {
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    grid-gap: 2rem;
    align-items: center;
    font-size: 2rem;
    padding: 150px 35px 150px 35px;
    ${customMedia.lessThan("tablet")`
            grid-template-columns: 100%;
            text-align: center;
            padding: 5rem;
            padding-left: 0;
            padding-right: 5rem;
        `}
  }

  // Video container
  .video-container {
    position: relative;
    ${customMedia.lessThan("tablet")`
    width: 450px;
    height: 450px;
    `}
  }

  // VisitVideo
  .visitVideo {
    ${customMedia.lessThan("tablet")`
    width: 250px;
    height: 250px;
    `}
  }

  // Video centered
  .centered {
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
  }
`;
