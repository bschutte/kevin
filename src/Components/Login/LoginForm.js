import React, { Component } from "react";
import styled from "styled-components";
import { Icon } from "react-icons-kit";
import { facebookSquare } from "react-icons-kit/fa/facebookSquare";
import { google } from "react-icons-kit/fa/google";
import "../../css/LoginForm.css";
import Facebook from "../Facebook/Facebook";

class LoginForm extends Component {
  switchOverlay() {
    const container = document.getElementById("container");
    container.classList.add("right-panel-active");
  }
  switchOverlayBack() {
    const container = document.getElementById("container");
    container.classList.remove("right-panel-active");
  }
  render() {
    return (
      <Body>
        <div class="container" id="container">
          <div class="form-container sign-up-container">
            <form action="#">
              <h1>Opprett bruker</h1>
              <div class="social-container">
                <Facebook></Facebook>
              </div>
              <span>Eller bruk email for registrering</span>
              <input type="text" placeholder="Navn" />
              <input type="email" placeholder="Email" />
              <input type="password" placeholder="Passord" />
              <button>Registrer</button>
            </form>
          </div>
          <div class="form-container sign-in-container">
            <form action="#">
              <h1>Logg på</h1>
              <div class="social-container">
                <Facebook></Facebook>
              </div>
              <span>or use your account</span>
              <input type="email" placeholder="Email" />
              <input type="password" placeholder="Passord" />
              <a href="#">Glemt passord?</a>
              <button>Logg på</button>
            </form>
          </div>
          <div class="overlay-container">
            <div class="overlay">
              <div class="overlay-panel overlay-left">
                <h1>Velkommen tilbake!</h1>
                <p>
                  For å fortsette å forbedre mattekarakterene logg på med oss
                </p>
                <button
                  onClick={this.switchOverlayBack}
                  class="ghost"
                  id="signIn"
                >
                  Logg på
                </button>
              </div>
              <div class="overlay-panel overlay-right">
                <h1>Hallo, student!</h1>
                <p>
                  Bli med på et matte eventyr med oss og få bedre karakterer
                </p>
                <button onClick={this.switchOverlay} class="ghost" id="signUp">
                  Registrer deg
                </button>
              </div>
            </div>
          </div>
        </div>
      </Body>
    );
  }
}

export default LoginForm;

const Body = styled.div`
  background: #f6f5f7;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding-top: 90px;
  height: 100vh;
  margin: -20px 0 50px;

  h1 {
    font-weight: bold;
  }

  p {
    font-size: 14px;
    font-weight: 100;
    line-height: 20px;
    letter-spacing: 0.5px;
    margin: 20px 0 30px;
  }

  span {
    font-size: 12px;
  }

  a {
    color: #333;
    font-size: 14px;
    text-decoration: none;
    margin: 15px 0;
  }
  button {
    border-radius: 20px;
    border: 1px solid #4C68FF;
    background: #4C68FF;
    color: #fff;
    font-size: 20px;
    font-weight: bold;
    padding: 12px 45px;
    text-transform: uppercase;
    transition: transform 80ms ease-in;
    outline: none;


`;
