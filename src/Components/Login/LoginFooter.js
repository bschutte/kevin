import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { Icon } from 'react-icons-kit';
import {ic_keyboard_arrow_right} from 'react-icons-kit/md/ic_keyboard_arrow_right'
import generateMedia from "styled-media-query";


class Footer extends Component {
    render() {
        return (
            <FooterContainer>
                <span style={{ marginLeft: "10%", fontSize: "1.125rem" }}>
                    Questions? <Link to="/">Kom i gang</Link>
                </span>
                <div className="footer-columns">
                    <ul>
                        <li>
                            <Link to="/">@AMatte</Link>
                        </li>
                        <li>
                            <Link>Brukervilkår</Link>
                        </li>
                        <li>
                            <Link>Personvern</Link>
                        </li>
                        <li>
                            <Link>Cookies</Link>
                        </li>
                        <li>
                            <Link>Copyright @AMatte - All Rights Reserved</Link>
                        </li>
                    </ul>
                </div>
            </FooterContainer>
        )
    }
}

export default Footer;

// Media


const FooterContainer = styled.footer`
justify-content: center;

    background: rgba(0,0,0,0.8);
    padding-top: 3rem;
    padding-bottom: 6rem;
    margin-top: 6rem;
    position: relative:
    z-index: 5;

    .footer-columns {
        width: 80%;
        margin: 1rem auto 0;
        color: #999;
        font-size: 0.9rem;
        overflow: auto;
        display: grid;
        grid-template:columns repeat(1,1fr);
        grid-gap: 2rem;
    }

    ul li {
        list-style: none;
        line-height: 2.5;
    }
    
    a {
        color: #000;
        font-size: 0.9rem;
    }

    a:hover {
        text-decoration: underline;
        color: #999;
        cursor: pointer;
    }

    p {
        text-decoration: underline;
        cursor: pointer;
    }


    // Prøv Nå Knapp
    .try-btn {
        background: var(--main-blue);
        border: 0.9px solid #333;
        border-radius: 25px;
        padding: 1rem;
        width: 8rem;
        cursor: pointer;
    }

`;