import React, { Component } from "react";
import styled from "styled-components";
import Navbar from "../Components/Navbar";
import Img from "../svg/laptop_no_screen.svg";
import { generateMedia } from "styled-media-query";
import { Link } from "react-router-dom";
import { Icon } from "react-icons-kit";
import { ic_keyboard_arrow_right } from "react-icons-kit/md/ic_keyboard_arrow_right";

class Landingpage extends Component {
  render() {
    return (
      <Body>
        <div class="hero">
          <Navbar></Navbar>
          <div class="hero-container">
            <div class="hero-content">
              <h1>Enkelere Matte</h1>
              <h2>Prøv oss idag!</h2>
              <div class="main-offer-btn">
                <Link to="/priser">
                  {" "}
                  <Button>
                    PRØV GRATIS{" "}
                    <Icon
                      className="Icon"
                      icon={ic_keyboard_arrow_right}
                      size={25}
                    />
                  </Button>
                </Link>
              </div>
            </div>
            <div class="video-container">
              <img src={Img} />
            </div>
          </div>
        </div>
      </Body>
    );
  }
}

export default Landingpage;

const customMedia = generateMedia({
  largeDesktop: "1350px",
  medDesktop: "1150px",
  tablet: "960px",
  smallTablet: "740px",
  mobile: "46em"
});

const Button = styled.div`
  display: inline-block;
  background: #273362;
  text-transform: uppercase;
  font-size: 25px;
  font-weight: bold;
  color: #fff;
  border: none;
  outline: none;
  width: 35%;
  padding: 25px 75px;
  border-radius: 0.1875rem;
  text-align: center;
  box-shadow: 0 1px 0 rgb(0, 0, 0, 0.45);
  transition: background 0.2s ease-in;
  cursor: pointer;
  text-decoration: none;
  display: flex;
  &:hover {
    background: #4259d5;
    text-decoration: none;
  }
`;

const Body = styled.div`
  .hero {
    height: 90vh;
    background-image: linear-gradient(
        rgba(0, 105, 233, 0.8) rgba(0, 105, 233, 0.8)
      ),
      url(../images/background.jpg);
    background-position: center;
    background-size: cover;
    margin: auto;
  }

  .hero-container {
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    grid-gap: 2rem;
    align-items: center;
    padding: 150px 35px 150px 35px;
    width: 100%;
  }

  .hero-content {
    margin-left: 40%;
    width: 100%;
  }
  .video-container {
    position: relative;
    width: 650px;
    height: 321px;
    ${customMedia.greaterThan("largeDesktop")`
    width: 790px;
    height: 425px;`}
  }

  h1 {
    font-size: 80px;
    font-weight: bold;
    color: #fff;
  }

  h2 {
    font-size: 50px;
    font-weight: 700;
    color: #fff;
    padding-bottom: 25px;
  }
  .Icon svg {
    color: #ff;

    vertical-align: bottom;
    margin-left: 1.5rem;
    ${customMedia.lessThan("smallTablet")`
    display:none !important;
    margin-left: 2px;
    ${customMedia.greaterThan("Tablet")`
    margin-right: 20px;`}
    `}
  }
`;
