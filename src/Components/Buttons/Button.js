import styled from "styled-components";

export const Button = styled.button`
  display: inline-block;
  background: #273362;
  text-transform: uppercase;
  font-weight: bold;
  color: #fff;
  border: none;
  outline: none;
  border-radius: 0.1875rem;
  text-align: center;
  box-shadow: 0 1px 0 rgb(0, 0, 0, 0.45);
  transition: background 0.2s ease-in;
  cursor: pointer;
  &:hover {
    background: #4259d5;
  }
`;
