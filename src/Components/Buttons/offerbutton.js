import styled from "styled-components";

export const buttonOffer = styled.button`
  color: #fff;
  background: #35db99;
  outline: none;
  border: none;
  border-radius: 0.5rem;
  font-size: 20px;
  font-weight: 700;
  text-align: center;
  box-shadow: 5px 5px 0 rgba(0, 0, 0, 0.45);
  transition: opacity 0.2s ease-in;
  cursor: pointer;
  text-decoration: none;
  &hover: {
    top: -70px;
    background: #35db99;
  }
`;
