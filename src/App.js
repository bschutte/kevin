import React from "react";
import "./css/App.css";
import Main from "./pages";
import Login from "./pages/Login";
import OmOss from "./pages/Om-oss";
import PriceTable from "./pages/Price-table";
import stripepayment from "./pages/stripepayment";

import { Switch, Route } from "react-router-dom";

function App() {
  return (
    <div>
      <Switch>
        <Route exact path="/" component={Main} />
        <Route path="/login" component={Login} />
        <Route path="/Om-oss" component={OmOss} />
        <Route path="/priser" component={PriceTable} />
        <Route path="/stripe" component={stripepayment} />
      </Switch>
    </div>
  );
}

export default App;
